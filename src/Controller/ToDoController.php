<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ToDo;

class ToDoController extends AbstractController
{
    /**
     * @Route("/todo", name="to_do")
     */
    public function index(): Response
    {
        $tasks = $this->getDoctrine()->getRepository(ToDo::class)->findAll();
        return $this->render('to_do/index.html.twig', [
            'tasks' => $tasks,
        ]);
    }

    /**
     * @Route("/todo/new", name="new")
     */
    public function new(): Response
    {
        return $this->render('to_do/new.html.twig');
    }

    /**
     * @Route("/todo/create", name="new_task")
     */
     public function create(Request $request): Response
     {

        $title = $request->request->get('title');

        $entityManager = $this->getDoctrine()->getManager();

        $task = (new ToDo())
            ->setTitle($title);

        $entityManager->persist($task);
        $entityManager->flush();

        return $this->redirectToRoute('to_do');

     }

     /**
      * @Route("/todo/delete/{id}", name="delete_task")
      */
     public function delete(ToDo $id){

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($id);
        $entityManager->flush();

        return $this->redirectToRoute('to_do');

     }

}
